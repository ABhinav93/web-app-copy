let icons;
let accessToken; 
let userEmail;

// let apiURL = "https://beta.myramed.in"
let apiURL = "https://api.myramed.in"

function handleToken() {
    let urlParams = new URLSearchParams(window.location.search);
    if(urlParams.has("access_token")){
        accessToken = urlParams.get("access_token")
    }
}

function fetchingCareStatus(){
    let statusFetch = new XMLHttpRequest();
    statusFetch.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            let resp = JSON.parse(statusFetch.responseText)
            if(resp.we_care_subscriptions.length && (resp.we_care_subscriptions[0].status == "INTERESTED")){
                document.querySelector('.acknowledge_container').classList.remove("hidden")
                document.querySelector('.email_container').classList.add("hidden")
                document.querySelector('.status_msg').classList.add("hidden")
                document.querySelector('.action_box').classList.add("hidden")
                document.querySelector(".sec_header").classList.add("hidden")
                document.querySelector(".blank_container").classList.remove("hidden")
            }
        }

    }

    if (accessToken) {
        statusFetch.open("GET", apiURL+"/we_care", true);
        // statusFetch.setRequestHeader("Authorization", `MRAuth 2131e74a0a274b47a4d04f98fc298771`);
		statusFetch.setRequestHeader("Authorization", `MRAuth ${accessToken}`);
		statusFetch.send();
	}
}

function subscriveforCare(){
    let subscribeParams = new FormData();
    subscribeParams.append("email",email)
    subscribeParams.append("scheme_id",35)
    subscribeParams.append("status","INTERESTED")
    let subscribeApi = new XMLHttpRequest();
    subscribeApi.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            console.log("function after successful api call")
            document.querySelector('.acknowledge_container').classList.remove("hidden")
            document.querySelector('.email_container').classList.add("hidden")
            document.querySelector('.status_msg').classList.add("hidden")
            document.querySelector('.action_box').classList.add("hidden")
            document.querySelector(".sec_header").classList.add("hidden")
            document.querySelector(".blank_container").classList.remove("hidden")
        }

    }

    if (accessToken) {
        subscribeApi.open("POST", apiURL+"/we_care", true);
        // subscribeApi.setRequestHeader("Authorization", `MRAuth 2131e74a0a274b47a4d04f98fc298771`);
		subscribeApi.setRequestHeader("Authorization", `MRAuth ${accessToken}`);
		subscribeApi.send(subscribeParams);
	}
}


function goToEmailSec(){
    if(accessToken){
        document.querySelector(".email_container").classList.remove("hidden")
        document.querySelector(".action_box").classList.add("hidden")
        document.querySelector(".status_msg").classList.add("hidden")
        document.querySelector(".sec_header").classList.add("hidden")
        document.querySelector(".blank_container").classList.remove("hidden")
        window.scrollTo(0,0);
        document.querySelector('.email_field').focus()
    }else{
        document.querySelector('.error_container').classList.remove("hidden")
        document.querySelector('.error_main').innerHTML = "User Not Authorised"
        document.querySelector('.error_message').innerHTML = "Not Authorised"
        setTimeout(function(){
            console.log(document.querySelector('.error_container').classList.add("hidden"))
        },2000)
        window.scrollTo(0,0)
    }
}

function checkEmailandApiCall(e){
    let emailInput = document.querySelector('.email_field')
    if(emailInput.validationMessage){
        document.querySelector('.error_container').classList.remove("hidden")
        document.querySelector('.error_main').innerHTML = "Email is not Valid"
        document.querySelector('.error_message').innerHTML = emailInput.validationMessage
        setTimeout(function(){
            console.log(document.querySelector('.error_container').classList.add("hidden"))
        },2000)
        return 0
    }
    email = emailInput.value;
    if(email){
        subscriveforCare()
    }
    
    
}

async function load() {
    icons = await import('./assets/icons.js');
    let mainIcon = icons.default.weCareIcon;
    let mlIcon = icons.default.medlifeIcon;
    let personsImage = icons.default.peopleIcon
    let handsImage = icons.default.handShakeIcon
    let docsIcon = icons.default.docsIcon
    let medicineIcon = icons.default.medicineIcon
    let labsIcon = icons.default.labsIcons 
    document.querySelector(".medlife_logo").innerHTML = mlIcon
    document.querySelector(".product_icon").innerHTML = mainIcon
    document.querySelector(".hands_icon").innerHTML = handsImage
    document.querySelector(".docs_icon").innerHTML = docsIcon
    document.querySelector(".medicine_icon").innerHTML = medicineIcon
    document.querySelector(".labs_icon").innerHTML = labsIcon
    document.querySelector(".person_image").innerHTML += personsImage 
    document.querySelector('#toEmail').addEventListener('click',goToEmailSec)
    document.querySelector('.email_submit').addEventListener('click',checkEmailandApiCall)
    handleToken()
    fetchingCareStatus()
}

window.addEventListener('load',load,true)

